

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Adri on 17/05/2017.
 */


//Kelas yang mengatur GUI program Owjek ini
public class Tampilan {
    private JFrame frameUtama;
    public Map map = new Map();
    public Tampilan() {
        setSemua();
    }

    //Method yang akan digunakan untuk melakukan inisialisasi tampilan
    private void setSemua(){
        frameUtama = new JFrame();
        frameUtama.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frameUtama.setTitle("OWJEK");
        JPanel asli = new JPanel();
        //Menggunakan sebuah Layout manager dengan pengaturan hanya satu baris dan 3 kolom dengan jarak vertikal horizontal 20
        GridLayout gridLayout = new GridLayout(1,3,20,20);
        asli.setLayout(gridLayout);
        //Melakukan set size
        frameUtama.setSize(800,1000);
        //3 Button akan diperlihatkan pada menu utama
        JButton buttonShow = new JButton("Show Map");
        JButton buttonOrder = new JButton("Order" );
        JButton buttonExit = new JButton("Exit");
        //Pemberian button button ini fungsi - fungsi tertentu
        buttonShow.addActionListener(new ShowMap());
        buttonExit.addActionListener(new Exit());
        buttonOrder.addActionListener(new Order());
        //Menambahkan Button button tadi ke dalam Sebuah panel
        asli.add(buttonShow);
        asli.add(buttonOrder);
        asli.add(buttonExit);
        //Panel tadi akan diletakkan di bagian atas dari Frame
        frameUtama.add(asli,BorderLayout.NORTH);
        frameUtama.setVisible(true);


    //Innner class yang memberikan fungsi kepada Button show Map
    }private class ShowMap implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            //Membuat sebuah frame baru untuk menunjukkan peta
            JFrame buatLiatinMap = new JFrame();
            GambarMap gambarMap = new GambarMap();
            buatLiatinMap.add(gambarMap);
            buatLiatinMap.setSize(buatLiatinMap.getMaximumSize().width,buatLiatinMap.getMaximumSize().height);
            buatLiatinMap.setVisible(true);
        }
    }
    //Inner kelas yang digunakan untuk menggambar Peta
    private class GambarMap extends JPanel {
        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            for (int i = 0; i <50 ; i++) {
                for (int j = 0; j < 100; j++) {
                    Color color  ;
                    //Menggunakan 2 kombnasi warna , jalan warna biru , selain itu putih
                    switch (map.getMap()[i][j]){
                        case '#':
                            color = Color.WHITE;
                            break;
                        case ' ':
                            color = Color.BLUE;
                            break;
                        default:
                            color = Color.WHITE;
                            break;
                    }
                    //Proses penggambaran
                    g.setColor(color);
                    g.fillRect(i*20,j*7,20,7);
                }
            }
        }

    } //Inner kelas untuk fungsi button Exit
    private class Exit implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            //Memunculkan dialog konfirmasi
            int konfirmasi = JOptionPane.showConfirmDialog(null,"Apakah anda yakin untuk keluar ?");
            //Jika user menjawab "Yes" maka program akan ditutup
            if(konfirmasi == 0) {
                JOptionPane.showMessageDialog(null,"Terima kasih sudah melakukan perjalanan bersama OWJEK");
                System.exit(0);
            }
        }
    }

    //Inner Kelas yang mengatur order
    private class Order  implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            //Membuat sebuah frame baru
            JFrame buatMap = new JFrame();
            //Membuat sebuah kelas gambar map untuk menggambar peta dan dimasukkan ke dalam Frame
            GambarMap gambarMap = new GambarMap();
            buatMap.add(gambarMap);
            buatMap.setSize(buatMap.getMaximumSize().width,buatMap.getMaximumSize().height);
            //Panel ultility akan muncul di bagian kanan dari Map
            JPanel ultility = new JPanel();
            JPanel ultilityUtara = new JPanel();
            //Layout Manager untuk mengatur bagian atas dari panel ini
            GridLayout utara = new GridLayout(2,2,5,5);
            ultilityUtara.setLayout(utara);
            //Tempat memasukkan input
            JLabel inputAsal = new JLabel("Masukan asal");
            JTextField asal = new JTextField();
            JLabel tujuanNya = new JLabel("Masukkan Tujuan");
            JTextField tujuan = new JTextField();
            //Memaskkan ke dalam panel atas
            ultilityUtara.add(inputAsal);
            ultilityUtara.add(asal);
            ultilityUtara.add(tujuanNya);
            ultilityUtara.add(tujuan);
            //Panel yang akan digunakan untuk memperlihatkan detail perjalanan
            JPanel tengah = new JPanel();
            GridLayout diRadioButton = new GridLayout(1,3,5,2);
            JPanel buatRadioButton = new JPanel();
            buatRadioButton.setLayout(diRadioButton);
            JTextArea hasilPerjalanan = new JTextArea();
            hasilPerjalanan.setEditable(false);
            //Pilihan Tipe Owjek
            JRadioButton regular = new JRadioButton("Reguler");
            JRadioButton sporty = new JRadioButton("Sporty");
            JRadioButton exclusive = new JRadioButton("Exclusive");
            //Semua radio button akan dimasukkan ke dalam Button Group
            ButtonGroup pilihan = new ButtonGroup();
            pilihan.add(regular);
            pilihan.add(sporty);
            pilihan.add(exclusive);
            PilihanButton tekan = new PilihanButton();
            //Membuat Fungsinalitas dari masing - masing radio button dengan sebuah inner class
            regular.addActionListener(tekan);
            sporty.addActionListener(tekan);
            exclusive.addActionListener(tekan);
            //Memasukkan semua radio button ke dalam panel
            buatRadioButton.add(regular);
            buatRadioButton.add(sporty);
            buatRadioButton.add(exclusive);
            //Pengaturan posisi
            tengah.setLayout(new BorderLayout());
            tengah.add(hasilPerjalanan,BorderLayout.CENTER);
            tengah.add(buatRadioButton,BorderLayout.SOUTH);
            JPanel selatan = new JPanel();
            //Pengaturan posisi button dengan GridLayout
            GridLayout letakkanButton = new GridLayout(1,2,6,0);
            selatan.setLayout(letakkanButton);
            //Inisialisasi button untuk melakukan perjalanan
            JButton submit = new JButton("GO !!");
            JButton clear = new JButton("Perjalanan Lain");
            //Fungsi dari Button perjalanan lain, mereset seluruh field yang ada di atas
            clear.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    asal.setText("");
                    tujuan.setText("");
                    hasilPerjalanan.setText("");
                }
            });
            selatan.add(submit);
            selatan.add(clear);
            ultility.setLayout(new BorderLayout());
            ultility.add(ultilityUtara,BorderLayout.NORTH);
            ultility.add(tengah,BorderLayout.CENTER);
            ultility.add(selatan,BorderLayout.SOUTH);
            //Fungsi dari button Go untuk memulai perjalanan berdasarkan semua field dan pilihan yang ada
            //Button ini juga akan mencetak detail perjalanan pada sebuah Text Area
            submit.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    hasilPerjalanan.setText("");
                    if(tekan.getTypePilihan().equalsIgnoreCase("reguler")){
                        OWJEK_Reguler ojekku = new OWJEK_Reguler();
                        ojekku.getCost(asal.getText(),tujuan.getText());
                        if(ojekku.isJalaninKagak())
                            ojekku.cetakDetail();
                        for(String o : ojekku.detailnya){
                            hasilPerjalanan.append(o);
                        }
                        MazeDigambar bikin = new MazeDigambar(ojekku.satuPerjalanan());
                        buatMap.remove(gambarMap);
                        buatMap.add(bikin);
                        bikin.repaint();
                    }else if (tekan.getTypePilihan().equalsIgnoreCase("sporty")){
                        Sporty ojekku = new Sporty();
                        ojekku.getCost(asal.getText(),tujuan.getText());
                        if(ojekku.isJalaninKagak())
                            ojekku.cetakDetail();
                        for(String o : ojekku.detailnya){
                            hasilPerjalanan.append(o);
                        }
                        MazeDigambar bikin = new MazeDigambar(ojekku.satuPerjalanan());
                        buatMap.remove(gambarMap);
                        buatMap.add(bikin);
                        bikin.repaint();
                    }else if (tekan.getTypePilihan().equalsIgnoreCase("exclusive")){
                        Exclusive ojekku = new Exclusive();
                        ojekku.getCost(asal.getText(),tujuan.getText());
                        if (ojekku.isJalaninKagak())
                            ojekku.cetakDetail();
                        for(String o : ojekku.detailnya){
                            hasilPerjalanan.append(o);
                        }
                        //Gambar peta akan diUpdate berdasarkan hasil perhitungan
                        MazeDigambar bikin = new MazeDigambar(ojekku.satuPerjalanan());
                        buatMap.remove(gambarMap);
                        buatMap.add(bikin);
                        bikin.repaint();
                    }
                }
            });
            buatMap.add(ultility,BorderLayout.EAST);
            buatMap.setVisible(true);
        }


    }
    //Fungsinalitas dari Radio Button
    private class PilihanButton implements ActionListener{
        private String typePilihan;



        @Override
        public void actionPerformed(ActionEvent e) {
            setTypePilihan(e.getActionCommand());
        }

        public String getTypePilihan() {
            return typePilihan;
        }

        public void setTypePilihan(String typePilihan) {
            this.typePilihan = typePilihan;
        }
    }
    //Kelas yang fungsinya hampir sama dengan Peta yang ada di atas, namun kelas ini
    // digunakan untuk menggambar ulang dengan keterangan yang sebelumnya tidak ada
    private class MazeDigambar extends JPanel{
        private Maze ygAkanDigambar;
        public MazeDigambar(Maze gambar){
            this.ygAkanDigambar = gambar;
        }
        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            for (int i = 0; i <50 ; i++) {
                for (int j = 0; j < 100; j++) {
                    Color color  ;
                    //Warna yang digunakan sama seperti sebelumnya ditambah warna : kuning untuk menunjukkan path ,Hijau posisi awal dan merah tujuan
                    switch (ygAkanDigambar.getMap()[i][j]){
                        case '#':
                            color = Color.WHITE;
                            break;
                        case ' ':
                            color = Color.BLUE;
                            break;
                        case 'S':
                            color = Color.GREEN;
                            break;
                        case 'F' :
                            color = Color.RED;
                            break;
                        default:
                            color = Color.YELLOW;
                            break;
                    }
                    g.setColor(color);
                    g.fillRect(i*20,j*7,20,7);
                }
            }
        }
    }
}
