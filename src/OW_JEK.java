/**
 * Created by Adri on 19/04/2017.
 */

//Class yang merepresentasikan Ow-Jek
public class OW_JEK  {
    private int minYearAllowed;
    private int costPerKm;
    private int kmSelanjutnya;
    private double hargaPromo;
    private int hargaTotal;
    private boolean jalaninKagak;
    private Maze satuPerjalanan = new Maze();
//Konstruktor
    public OW_JEK(int minYearAllowed,  int costPerKm) {
        this.minYearAllowed = minYearAllowed;
        this.costPerKm = costPerKm;
    }
//Setter dan getter
    public int getCostPerKm() {
        return costPerKm;
    }

    public void setCostPerKm(int costPerKm) {
        this.costPerKm = costPerKm;
    }

    public Maze satuPerjalanan() {
        return satuPerjalanan;
    }

//Method yang berguna untuk mencetak terima kasih setiap selesai perjalanan
    public void cetakTerimaKasih(){
        System.out.println("Terima kasih sudah melakukan perjalanan dengan OW-JEK");
    }
//Method yang akan di override oleh sub class untuk kebutuhan cetak detail perjalanan
    public void cetakDetail(){
        System.out.println("Harga dan lain-lain");
    }
//Setter Getter
    public double getHargaPromo() {
        return hargaPromo;
    }

    public void setHargaPromo(double hargaPromo) {
        this.hargaPromo = hargaPromo;
    }
    public void hargaSelanjutnya(int harga){
        kmSelanjutnya = harga;
    }

    public int getSelanjutnya(){
        return kmSelanjutnya;
    }

    public int getHargaTotal() {
        return hargaTotal;
    }

    public void setHargaTotal(int hargaTotal) {
        this.hargaTotal = hargaTotal;
    }
//Method yang akan digunakan untuk melihat apakah method cetakDetail dijalankan atau tidak(Karena kesalahan dsb)
    public boolean isJalaninKagak() {
        return jalaninKagak;
    }
//Method untuk mengatur nilai boolean dari jalaninKagak
    public void setJalaninKagak(boolean jalaninKagak) {
        this.jalaninKagak = jalaninKagak;
    }
}
