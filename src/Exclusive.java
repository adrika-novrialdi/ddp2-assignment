import java.util.ArrayList;

/**
 * Created by Adri on 27/04/2017.
 */

//Kelas Ow-Jek Exclusive , sub class OW-JEK dan mengimplementasikan interface Owjek
public class Exclusive extends OW_JEK implements Owjek {
    private int minCC = 500;
    private double protectionCost = 0.05;
    private int fixedCost = 10000;
    private double hargaProteksisaja ;
    public ArrayList<String> detailnya = new ArrayList<>();

    public Exclusive() {
        super(2016, 5000);
    }

    public double getHargaProteksisaja() {
        return hargaProteksisaja;
    }

    public void setHargaProteksisaja(double hargaProteksisaja) {
        this.hargaProteksisaja = hargaProteksisaja;
    }
//Method yang mengembalikan potongan harga
    @Override
    public int getPromo(int harga) {
        setHargaPromo(0.5*harga);
        return (int) getHargaPromo();
    }
//Method untuk menghitung dan mengembalikan Harga Proteksi
    public int hargaProteksi(int harga){
        setHargaProteksisaja ( protectionCost*harga);
        return (int)getHargaProteksisaja();
    }
//Menhitung pembayaran untuk perjalanan
    @Override
    public void getCost(String from, String to) {
            satuPerjalanan().getDestination(from,to);
            //Memastikan validitas input
            if (satuPerjalanan().validate()){
                satuPerjalanan().setMapandGetDistance();
                setJalaninKagak(true);
                double jaraknya = satuPerjalanan().getDistance();
                hargaSelanjutnya(((int)jaraknya)*getCostPerKm());
                int hargaTanpaProteksi = fixedCost + ((int)(jaraknya*getCostPerKm()))- getPromo(fixedCost +
                        ((int)(jaraknya*getCostPerKm())));
                int hargaProteksi = hargaProteksi(hargaTanpaProteksi);
                setHargaTotal(hargaTanpaProteksi + hargaProteksi);
            }else {
                //Jika input tidak valid
                satuPerjalanan().jikaExcept();
                setJalaninKagak(false);

            }




    }
//Mencetak detail perjalanan dan transaksi
    @Override
    public void cetakDetail() {
        detailnya.add("Terima kasih sudah melakukan perjalanan bersama OWJEK+\n");
        detailnya.add(String.format("[Jarak] %.2f KM\n",satuPerjalanan().getDistance()));
        detailnya.add(String.format("[TipeO] %s\n","Exclusive"));
        detailnya.add(String.format("[Fixed] Rp.%d(+)\n",fixedCost));
        detailnya.add(String.format("[KMSel] Rp.%d(+)\n",getSelanjutnya()));
        detailnya.add(String.format("[Promo] Rp.%d(-)\n",(int)getHargaPromo()));
        detailnya.add(String.format("[Prtks] Rp.%d(+)\n",(int)getHargaProteksisaja()));
        detailnya.add(String.format("[Total] Rp.%d\n",getHargaTotal()));
    }
}
