import java.util.ArrayList;

/**
 * Created by Adri on 25/04/2017.
 */

//Kelas yang merepresentasikan koordinat (x,y)
public class Grid {
    private int x;
    private int y;


    //Konstruktor
    public Grid(int x, int y) {
        this.x = x;
        this.y = y;
    }

//Setter Getter
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    //Override method equals untuk membandingkan antar grid
    @Override
    public boolean equals(Object obj) {
        return getX() == ((Grid)obj).getX() && getY() == ((Grid) obj).getY();
    }

    public int hashCode(){
        int hash = x + y;
        if (x > y){
            hash += x;
        }
        return hash;
    }
}
