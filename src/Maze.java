/**
 * Created by Adri on 20/04/2017.
 */
import jdk.nashorn.internal.scripts.JO;

import javax.swing.*;
import java.util.*;
//Kelas yang merepresentasikan Maze , sub class dari Map
public class Maze extends Map {
    private int koordinatXStart = 0;
    private int koordinatYstart = 0;
    private int koordinatXFinish = 0;
    private int koordinatYFisnish = 0;
    private double distance ;
    private String from;
    private String finish;
    private String bukanJalan;


//Method yang akan mengatur koordinat (x1,y1) dan (X2,y2) yang merupakan koordinat tempat jemput dan tujuan
    public void getDestination(String fromStart,String to){
        String xStart = fromStart.substring(0,2).toUpperCase();
        String yStart = fromStart.substring(2,fromStart.length()).toUpperCase();
        String xFinished = to.substring(0,2).toUpperCase();
        String yFinished = to.substring(2,to.length()).toUpperCase();
        from = fromStart;
        finish = to;
        koordinatXStart = getCoordinate(xStart);
        koordinatYstart = getCoordinate(yStart);
        koordinatXFinish = getCoordinate(xFinished);
        koordinatYFisnish = getCoordinate(yFinished);

    }
//Method yang akan mengubah String inputan menjadi x,y dalam integer
    private static int getCoordinate(String s) {
        int result = 0;
        char sChar = s.toLowerCase().charAt(0);
        result += Integer.parseInt(s.substring(1));

        if ((int) sChar >= 97 && 101 >= (int) sChar) {
            result += 10 * (sChar - 97);
        } else {
            result += 10 * (sChar - 113);
        }
        return result;
    }

//Method untuk mencari rute terpendek ke tujuan,mengembalikan solusi berupa ArraList<Grid>
    public ArrayList<Grid> solve(Grid start, Grid finish){
        ArrayDeque<Grid> frontier = new ArrayDeque<>();
        HashMap<Grid, Grid> cameFrom = new HashMap<>();
        ArrayList<Grid> route = new ArrayList<>();
        boolean searching = true;

        frontier.add(start);
        cameFrom.put(start,null);
        Grid current;

        while (!frontier.isEmpty() && searching){
            current = frontier.poll();

            if (current.equals(finish)){
                searching = false;

            }
            for(Grid next : neighbours(current)){
                if(!cameFrom.containsKey(next)){
                    frontier.add(next);
                    cameFrom.put(next, current);
                }
            }
        }

        Grid temp = finish;
        while (!temp.equals(start)){
            Grid tempJuga = cameFrom.get(temp);
            route.add(tempJuga);
            temp = tempJuga;
        }
        route.add(start);

        return route;
    }
//Method untuk memastikan koordinat yang dilewati adalah jalan dan bukan tembok
    public boolean isRoad(Grid id){
        return !(getMap()[id.getX()][id.getY()].equals('#'));
    }
//Method untuk mencari semua neighbour dari suatu grid yang mungkin (bukan tembok atau bangunan)
    public ArrayList<Grid> neighbours(Grid id){
        int x = id.getX();
        int y = id.getY();
        int[][] result = {{x+1,y},{x,y-1},{x-1,y},{x,y+1}};
        ArrayList<Grid> hasilnya = new ArrayList<>();
        Grid inElement;
        for(int[] i : result){
            inElement = new Grid(i[0],i[1]);
            if(isRoad(inElement)){
                hasilnya.add(inElement);
            }
        }
        return hasilnya;
    }
//Untuk mengubah Koordinat koordinat solusi menjadi titik di map
    public void setMapandGetDistance(){
        ArrayList<Grid> hasil = solve(new Grid(koordinatXStart,koordinatYstart),new Grid(koordinatXFinish,koordinatYFisnish));
        int counter = 0;
        for(Grid x : hasil){
            set('.',x.getX(),x.getY());
            counter +=1;
        }
        set('F',hasil.get(0).getX(),hasil.get(0).getY());
        getMap()[hasil.get(hasil.size()-1).getX()][hasil.get(hasil.size()-1).getY()] = 'S';
        distance = (counter*0.1)-0.1;

    }
//Method untuk memastikan suatu input valid atau tidak
    public boolean validate(){
        if(!isRoad(new Grid(koordinatXStart,koordinatYstart))){
            bukanJalan = from+ " bukan jalan !";
            return false;
        }if(!isRoad(new Grid(koordinatXFinish,koordinatYFisnish))){
            bukanJalan =  finish + " bukan jalan !";
            return false;
        }
        if(isRoad(new Grid(koordinatXStart,koordinatYstart))|| isRoad(new Grid(koordinatXFinish,koordinatYFisnish))){
            return true;

        }return true;
    }

    public double getDistance() {
        return distance;
    }
//Jika input tidak valid
    public void jikaExcept(){
        JOptionPane.showMessageDialog(null,String.format(bukanJalan));
    }


}
