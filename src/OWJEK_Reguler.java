import java.util.ArrayList;

/**
 * Created by Adri on 20/04/2017.
 */


//Ow-Jek yang merepresentasikan OW-Jek regular,sub kelas dari OW_JEK dan mengimplementasikan interface Owjek
public class OWJEK_Reguler extends OW_JEK implements Owjek {
    private int first2KmCost = 3000;
    public ArrayList<String> detailnya = new ArrayList<>();


//Konstruktor
    public OWJEK_Reguler() {
        super(2012,1000);

    }
    //Method yang akan mengatur dan mengembalikan langsung jumlah potongan harga
    public int getPromo(int harga){
        setHargaPromo(0.4*harga);
        return (int) getHargaPromo();

    }

//Method yang akan menghitung berapa bayaran yang harus dibayar
    public void getCost(String from,String to ){


        satuPerjalanan().getDestination(from,to);
        //Menentukan validitas inputan , jika bukan jalan , maka perjalanan tidak dapat dilakukan
        if(satuPerjalanan().validate()){
            setJalaninKagak(true);
            satuPerjalanan().setMapandGetDistance();
            double jaraknya = satuPerjalanan().getDistance();
            if (jaraknya == 0){
                setHargaTotal(0);
            } if(jaraknya <= 2.0){
                setHargaTotal( first2KmCost - getPromo(first2KmCost));
            }if(jaraknya > 2.0){
                double jarakSisa = jaraknya - 2.0;
                hargaSelanjutnya(((int)jarakSisa)*getCostPerKm());
                if(jaraknya <= 6){
                    setHargaTotal (first2KmCost + ((int)jarakSisa)*getCostPerKm() -
                            getPromo(first2KmCost + ((int)(jarakSisa)*getCostPerKm())));
                }else {
                    setHargaTotal (first2KmCost + ((int)(jarakSisa*getCostPerKm())) -
                            getPromo(first2KmCost + ((int)((6.0*getCostPerKm())))));
                }
            }

        }else {
            //Mencetak alasan kenapa perjalanan tidak dapat dilakukan dan tidak ada bayaran
            satuPerjalanan().jikaExcept();
            setJalaninKagak(false);
        }

    }

//Method untuk mencetak detail perjalanan dan transaksi
    @Override
    public void cetakDetail() {
        detailnya.add("Terima kasih sudah melakukan perjalanan bersama OWJEK+\n");
        detailnya.add(String.format("[Jarak] %.2f KM\n",satuPerjalanan().getDistance()));
        detailnya.add(String.format("[TipeO] %s\n","Reguler"));
        detailnya.add(String.format("[2KmPe] Rp.%d(+)\n",first2KmCost));
        detailnya.add(String.format("[KMSel] Rp.%d(+)\n",getSelanjutnya()));
        detailnya.add(String.format("[Promo] Rp.%d(-)\n",(int)getHargaPromo()));
        detailnya.add(String.format("[Total] Rp.%d\n",getHargaTotal()));
    }


}
