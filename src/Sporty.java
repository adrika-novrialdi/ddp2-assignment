import java.util.ArrayList;

/**
 * Created by Adri on 27/04/2017.
 */


//Kelas yang merepresentasikan OW-JEk Sporty, sub class OW-JEK dan mengimplementasikan interface Owjek
public class Sporty extends OW_JEK implements Owjek {
    private int first5KmCost = 20000;
    private double protectionCost = 0.1;
    private double hargaProteksiSaja ;
    public ArrayList<String>  detailnya =  new ArrayList<>();

//Konstruktor
    public Sporty() {
        super(2015, 3000);

    }
//Method yang mengembalikan jumlah uang proteksi
    public int hargaProteksi(int harga){
        setHargaProteksisaja ( protectionCost*harga);
        return (int)getHargaProteksisaja();
    }

    public double getHargaProteksisaja() {
        return hargaProteksiSaja;
    }

    public void setHargaProteksisaja(double hargaProteksisaja) {
        this.hargaProteksiSaja = hargaProteksisaja;
    }
//Method untuk menghitung potongan harga
    @Override
    public int getPromo(int harga) {
        setHargaPromo(0.6*harga);
        return (int) getHargaPromo();
    }
//Method untuk menghitung bayaran
    @Override
    public void getCost(String from, String to) {
        satuPerjalanan().getDestination(from,to);
        //Memastikan Validitas input ( Sama seperti tipe lainnya)
        if (satuPerjalanan().validate()){
            setJalaninKagak(true);
            satuPerjalanan().setMapandGetDistance();
            double jaraknya = satuPerjalanan().getDistance();
            if(jaraknya <= 5.0){
                setHargaTotal( first5KmCost - getPromo(first5KmCost)+
                        hargaProteksi(first5KmCost - (int)getPromo(first5KmCost)));
            }if(jaraknya >= 5.0){
                double jarakSisa = jaraknya - 5.0;
                hargaSelanjutnya(((int)jarakSisa)*getCostPerKm());
                if(jaraknya <= 8){
                    int hargaTanpaProteksi = first5KmCost + ((int)jarakSisa)*getCostPerKm() -
                            getPromo(first5KmCost + ((int)(jarakSisa)*getCostPerKm()));
                    int hargaProteksi = hargaProteksi(hargaTanpaProteksi);
                    setHargaTotal(hargaTanpaProteksi + hargaProteksi);
                }if(jaraknya > 8) {
                    int hargaTanpaProteksi = first5KmCost + ((int)(jarakSisa*getCostPerKm())) -
                            getPromo(first5KmCost + ((int)((8.0*getCostPerKm()))));
                    int hargaProteksi = hargaProteksi(hargaTanpaProteksi);
                    setHargaTotal(hargaTanpaProteksi+hargaProteksi);
                }if (jaraknya == 0){
                    setHargaTotal(0);
                }
            }
        }else {
            satuPerjalanan().jikaExcept();
            setJalaninKagak(false);
        }

    }


//Method untuk mencetak Detail perjalanan dan transaksi
    @Override
    public void cetakDetail() {
        detailnya.add("Terima kasih sudah melakukan perjalanan bersama OWJEK+\n");
        detailnya.add(String.format("[Jarak] %.2f KM\n",satuPerjalanan().getDistance()));
        detailnya.add(String.format("[TipeO] %s\n","Sporty"));
        detailnya.add(String.format("[5KmPe] Rp.%d(+)\n",first5KmCost));
        detailnya.add(String.format("[KMSel] Rp.%d(+)\n",getSelanjutnya()));
        detailnya.add(String.format("[Promo] Rp.%d(-)\n",(int)getHargaPromo()));
        detailnya.add(String.format("[Prtks] Rp.%d(+)\n",(int)getHargaProteksisaja()));
        detailnya.add(String.format("[Total] Rp.%d\n",getHargaTotal()));
    }
}
